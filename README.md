# Скачать OKD OpenShift v3.11.0

Для скачивания доступны следующие архивы:
- OpenShift Client (Linux 64) 56.5Мб [скачать](https://gitlab.com/agrocoder/okd/-/raw/main/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz)
- OpenShift Client (MacOS) 55.9Мб [скачать](https://gitlab.com/agrocoder/okd/-/raw/main/openshift-origin-client-tools-v3.11.0-0cbc58b-mac.zip)
- OpenShift Client (Windows) 56.2Мб [скачать](https://gitlab.com/agrocoder/okd/-/raw/main/openshift-origin-client-tools-v3.11.0-0cbc58b-windows.zip)
- OpenShift Server (Linux 64) 228.1Мб [скачать](https://gitlab.com/agrocoder/okd/-/raw/main/openshift-origin-server-v3.11.0-0cbc58b-linux-64bit.tar.gz)

![Вход в админку](https://gitlab.com/agrocoder/okd/-/wikis/uploads/0480947191a954d924fe5ca5c8d354a0/image.png "Вход в админку")

**Оригинальные названия архивов:**
- openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz
- openshift-origin-client-tools-v3.11.0-0cbc58b-mac.zip
- openshift-origin-client-tools-v3.11.0-0cbc58b-windows.zip
- openshift-origin-server-v3.11.0-0cbc58b-linux-64bit.tar.gz

![Начало работы в админке](https://gitlab.com/agrocoder/okd/-/wikis/uploads/d2de1fea594f400192fa05659b1a50e6/image.png "Начало работы в админке")

OKD-это дистрибутив Kubernetes, оптимизированный для непрерывной разработки приложений и многопользовательского развертывания.

OKD внедряет Kubernetes и расширяет его с помощью безопасности и других интегрированных концепций.

OKD добавляет инструменты, ориентированные на разработчиков и операции, поверх Kubernetes, чтобы обеспечить быструю разработку приложений, простое развертывание и масштабирование, а также долгосрочное обслуживание жизненного цикла для небольших и больших команд.

OKD также упоминается как Origin в GitHub и в документации.

OKD является родственным дистрибутивом Kubernetes для Red Hat OpenShift.
